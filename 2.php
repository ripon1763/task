<?php

function printDepth($array, $level = 1){
    foreach($array as $key => $value){
        if(is_array($value)){
            printDepth($value, $level + 1);
			echo $key . " " . $level, '<br>';
        } else{
            echo $key . " " . $level, '<br>';
        }
    }
}


class Person{
public function __construct($first_name, $last_name, $father) {
	$this->first_name = $first_name;
	$this->last_name = $last_name;
	$this->father = $father;
	}
}

$person_a = new Person("User", "1", NULL);
$person_b = new Person("User", "2", $person_a);

$data = array (
"key1" => 1,
"key2" => array (
		"key3" => 1,
		"key4" => array ("key5" => 4,
		"User" => json_decode(json_encode($person_b), true)
	),
  ),
);

 
 printDepth($data);

?>